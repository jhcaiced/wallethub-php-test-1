# PHP Exercise


## Image Resize Script

A simple CLI script that allows the resize of image files, should be called something like

`php scripts/imageResize.php -i <input.jpg> -o <output.jpg> [-w <width>] [-h <height>]`

## Facebook Proxy
A simple example of using an HTTP client to proxy requests to Facebook API

### Running the local server
First, install the dependencies for the repository

`composer install`

Then, just use the PHP built in server, something like:

`php -S localhost:9000 -t public/`

Any request sent to this local server will be redirected to Facebook Graph API with the same URI and same parameters either using GET or POST methods
i.e. http://localhost:9000/820882001277849 will forward the request to https://graph.facebook.com/820882001277849 (you still need to send the proper access token and other parameters)

## Tests
All code is covered by tests, just run phpunit in the top directory, in order to test correctly the proxy to the facebook graph api, 
you need to set the environment vars TEST_API_URL and TEST_ACCESS_TOKEN to correct values.

```
export TEST_API_URL=http://localhost:9000
export TEST_ACCESS_TOKEN=<token>
phpunit
```

