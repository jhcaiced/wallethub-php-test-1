<?php
use GetOpt\GetOpt;
use App\Helpers\ImageResize;

require_once dirname(__FILE__) . '/../vendor/autoload.php';
$getopt = new GetOpt([
    ['i', 'input', GetOpt::REQUIRED_ARGUMENT, 'Input file'],
    ['o', 'output', GetOpt::REQUIRED_ARGUMENT, 'Output file'],
    ['w', 'width', GetOpt::OPTIONAL_ARGUMENT, 'New image width'],
    ['h', 'height', GetOpt::OPTIONAL_ARGUMENT, 'New image height']
]);
$getopt->process();

$inputFile = $getopt->getOption('input');
if (empty($inputFile) || !file_exists($inputFile)) {
    throw new Exception('Cannot open input file');
}

$outputFile = $getopt->getOption('output');
if (empty($outputFile)) {
    throw new Exception('Output file is a required parameter');
}

$imageResize = new ImageResize();
return $imageResize->resize($getopt->getOptions());
