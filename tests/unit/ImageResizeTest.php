<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use App\Helpers\ImageResize;

final class ImageResizeTest extends TestCase
{
    public function testImageResizeNoParams()
    {
        $this->expectException('Exception');
        $image = new ImageResize();
        $image->resize($this->defaultOptions());
    }

    public function testImageResizeByWidth()
    {
        $options= array_merge($this->defaultOptions(), [
            'width' => 444
        ]);
        $image = new ImageResize();
        $image->resize($options);
        $this->assertTrue(file_exists($options['output']));
        $imageSize = $this->getImageSize($options['output']);
        $this->assertEquals($imageSize['width'], $options['width']);
    }

    public function testImageResizeByHeight()
    {
        $options= array_merge($this->defaultOptions(), [
            'height' => 555
        ]);
        $image = new ImageResize();
        $image->resize($options);
        $this->assertTrue(file_exists($options['output']));
        $imageSize = $this->getImageSize($options['output']);
        $this->assertEquals($imageSize['height'], $options['height']);
    }

    public function testImageResize()
    {
        $options= array_merge($this->defaultOptions(), [
            'width' => 222,
            'height' => 333
        ]);
        $image = new ImageResize();
        $image->resize($options);
        $this->assertTrue(file_exists($options['output']));
        $imageSize = $this->getImageSize($options['output']);
        $this->assertEquals($imageSize['width'], $options['width']);
        $this->assertEquals($imageSize['height'], $options['height']);
    }

    protected function defaultOptions()
    {
        return [
            'input' => __DIR__ . '/sample/image.jpg',
            'output' => sys_get_temp_dir() . '/output.jpg'
        ];
    }

    protected function getImageSize($fileName)
    {
        $imageSize = getimagesize($fileName);
        return [
            'width' => $imageSize[0],
            'height' => $imageSize[1]
        ];
    }
}
