<?php

namespace Test\Api;

final class FacebookProxyTest extends ApiTestCase
{
    public function testFacebookGet()
    {
        $response = $this->http->get(
            $this->baseUrl .
            '?id=' . 'http://www.imdb.com/title/tt2015381' .
            '&access_token=' . $this->getVar('TEST_ACCESS_TOKEN')
        );

        $options = [
        'id' => 'http://www.imdb.com/title/tt2015381',
        'access_token' => $this->getVar('TEST_ACCESS_TOKEN')
        ];

        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()['Content-Type'][0];
        $this->assertEquals('application/json;charset=utf-8', $contentType);

        $body = $this->jsonResponse($response);
        $this->assertTrue($body['status'] === 'ok');
        $this->assertTrue(isset($body['data']['url']));
    }

    public function testFacebookPost()
    {
        $options = [
            'id' => 'http://www.imdb.com/title/tt2015381',
            'access_token' => $this->getVar('TEST_ACCESS_TOKEN')
        ];
        $response = $this->http->post($this->baseUrl, ['form_params' => $options]);


        $this->assertEquals(200, $response->getStatusCode());

        $contentType = $response->getHeaders()['Content-Type'][0];
        $this->assertEquals('application/json;charset=utf-8', $contentType);

        $body = $this->jsonResponse($response);
        $this->assertTrue($body['status'] === 'ok');
        $this->assertTrue(isset($body['data']['url']));
    }
}
