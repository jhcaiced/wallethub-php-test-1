<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\ErrorLogHandler;

use App\Helpers\FacebookGraphClient;

require_once '../vendor/autoload.php';

$app = new \Slim\App;
$container = $app->getContainer();

$container['logger'] = function () {
    $logger = new Logger('logger');
    $logger->pushHandler(new StreamHandler('php://stderr'));
    $logger->pushHandler(new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, Logger::INFO));
    return $logger;
};

$app->map(
    ['GET', 'POST'],
    '/[{params:.*}]',
    function (Request $request, Response $response, array $args) use ($container) {
        $logger = $container['logger'];
        $client = new FacebookGraphClient();
        $result = $client->request(
            $request->getUri()->getPath(),
            $request->getParams()
        );
        $logger->debug($request->getUri()->getPath());
        $logger->debug(print_r($request->getParams(), true));
        $logger->debug(print_r($result, true));
        return $response->withJson($result);
    }
);
$app->run();
