<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class FacebookGraphClient
{
    const FB_GRAPH_URL = 'https://graph.facebook.com/v3.1';

    public function request($path, $params)
    {
        $client = new Client();
        try {
            $response = $client->request('POST', $this->getUrl($path), ['form_params' => $params]);
            return [
                'status' => 'ok',
                'data' => json_decode($response->getBody()->getContents())
            ];
        } catch (ClientException $e) {
            return ['error' => [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ]];
        }
    }

    protected function getUrl($path)
    {
        return self::FB_GRAPH_URL . $path;
    }
}
