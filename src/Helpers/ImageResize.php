<?php

namespace App\Helpers;

use Exception;

class ImageResize
{
    public function resize($options)
    {
        $image = new \Gumlet\ImageResize($options['input']);

        if (empty($options['width']) && empty($options['height'])) {
            throw new Exception('Either width or height is required');
        }

        if (!empty($options['width']) && !empty($options['height'])) {
            $image->resize($options['width'], $options['height']);
        }
        if (!empty($options['width']) && empty($options['height'])) {
            $image->resizeToWidth($options['width']);
        }
        if (empty($options['width']) && !empty($options['height'])) {
            $image->resizeToHeight($options['height']);
        }
        $image->save($options['output']);
        return $options['output'];
    }
}
